import logo from './logo.svg'
import './App.css'
import FunctionComponent from './DemoComponent/FunctionComponent'
import ClassComponent from './DemoComponent/ClassComponent'
import BTComponent from './BTComponent/BTComponent'
import BindingData from './BindingData/BindingData'
import RenderWithCondition from './RenderWithCondition/RenderWithCondition'
import HandleEvent from './HandleEvent/HandleEvent'
import StyleComponent from './StyleComponent/StyleComponent'
import RenderWithMap from './RenderWithMap/RenderWithMap'
import DemoState from './State/DemoState'
import DemoProps from './Props/DemoProps'
import BTShoeShop from './BTShoeShop/BTShoeShop'
import DemoRedux from './DemoRedux/DemoRedux'
import BTPhoneRedux from './BTPhoneRedux/BTPhoneRedux'
import BTMovieBooking from './BTMovieBooking/BTMovieBooking'

import { Route, Routes } from 'react-router-dom'
import NotFound from './pages/NotFound'
import HomePage from './pages/HomePage'
import MainLayout from './components/layouts/MainLayout'
import { PATH } from './constant/config'
import MovieDetail from './RenderWithMap/MovieDetail'
import { useRouters } from './router/routers'

//component lớn nhất trong dự án

// 2 loại component
// + class component (stateful) <2019 (lifecyle)
// + function component (stateless) 100%
// 1 component chỉ đc phép return về 1 thẻ jsx duy nhất
// attribute: viết theo quy tác camelCase

//JSX: Javascript XML => giúp các bạn viết html trong javascript

function App() {
    return (
        // <div></div>
        <div className="App">
            {/* <span className='title'>HELLO BC47</span>
            <p>nguyễn vieté hải</p>
            <h1></h1>

            <FunctionComponent></FunctionComponent>
            <ClassComponent></ClassComponent>

            <FunctionComponent /> */}
            {/* <Routes>
                <Route element={<MainLayout />}>
                    <Route index element={<HomePage />} />
                    <Route path={PATH.baiTapComponent} element={<BTComponent />} />
                    <Route path={PATH.bindingData} element={<BindingData />} />
                    <Route path={PATH.renderCondition} element={<RenderWithCondition />} />
                    <Route path={PATH.handleEvent} element={<HandleEvent />} />
                    <Route path={PATH.styledComponent} element={<StyleComponent />} />
                    <Route path={PATH.renderMap} element={<RenderWithMap />} />
                    <Route path={PATH.movieDetail} element={<MovieDetail />} />

                    <Route path={PATH.demoState} element={<DemoState />} />
                    <Route path={PATH.demoProps} element={<DemoProps />} />
                    <Route path={PATH.baiTapShoeShop} element={<BTShoeShop />} />

                    <Route path={PATH.redux}>
                        <Route index element={<DemoRedux />} />
                        <Route path={PATH.baiTapPhone} element={<BTPhoneRedux />} />
                        <Route path={PATH.baiTapMovie} element={<BTMovieBooking />} />
                    </Route>
                    <Route path="*" element={<NotFound />} />
                </Route>
            </Routes> */}

            {useRouters()}
        </div>
    )
}

export default App
